unavi:
	./unavi.py

ini:
	@sed    's/password\ =\ .*$$/password = fafafa/g'       unavi.ini >examples/unavi.ini
	@sed -i 's/ssh_tunel\ =\ .*$$/ssh_tunel = server/g'                examples/unavi.ini
	@sed -i 's/server_name\ =\ .*$$/server_name = server/g'            examples/unavi.ini
	@sed -i 's/url\ =\ .*$$/url = http:\/\/example.com\//g'            examples/unavi.ini
