#!/usr/bin/env python3

# This script comes with ABSOLUTELY NO WARRANTY, use at own risk
# Copyright (C) 2022 Osiris Alejandro Gomez <osiux@osiux.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import time
from backports import configparser
import subprocess
import inspect

from splinter import Browser
from pprintpp import pprint as pp

import yaml

global browser
global config
global awx
global pve
global git
global xte_keydown_ctrl
global xte_key_n
global xte_key_plus
global xte_keyup_ctrl


browser = Browser("firefox")
config = configparser.ConfigParser()
config.read("unavi.ini")
awx = config["awx"]
pve = config["pve"]
git = config["git"]


def go_sleep(seconds):
    if seconds:
        print(inspect.stack()[1][3])
        time.sleep(float(seconds))


def ssh_tunel():
    ssh_pve_tunel = ["ssh", "-qfNC", pve["ssh_tunel"]]
    subprocess.call(ssh_pve_tunel)
    go_sleep(pve["time_ssh_tunel"])

    ssh_awx_tunel = ["ssh", "-qfNC", awx["ssh_tunel"]]
    subprocess.call(ssh_awx_tunel)
    go_sleep(awx["time_ssh_tunel"])


with open("unavi.yml", "r") as f:
    try:
        data = yaml.safe_load(f)
    except yaml.YAMLError as exc:
        print(exc)

link = None


for action in data["actions"]:

    pp(action)

    go_sleep(action["sleep"])

    if action["type"] == "subprocess":
        if action["method"] == "xte":
            if action["action"] == "call":
                subprocess.call(["xte", action["value"]])

    if action["type"] == "url":
        if action["method"] == "visit":
            browser.visit(action["value"])

    if action["type"] == "fill":
        if action["method"] == "find_by_id":
            browser.find_by_id(action["value"])
        if action["method"] == "find_by_css":
            browser.find_by_css(action["value"])
        browser.fill(action["value"], action["fill"])

    if action["type"] == "link":
        if action["method"] == "find_by_text":
            link = browser.find_by_text(action["value"])

        if action["method"] == "find_by_css":
            link = browser.find_by_css(action["value"])

        if action["method"] == "find_by_id":
            link = browser.find_by_id(action["value"])

    if action["type"] == "button":
        if action["method"] == "find_by_css":
            link = browser.find_by_css(action["value"])
        if action["method"] == "find_by_id":
            link = browser.find_by_id(action["value"])

    pp(browser.title)

    if link:
        if action["action"] == "click":
            link.click()

    if action["type"] == "browser" and action["action"] == "quit":
        browser.quit()
