# `unavi`

## PoC Overview

Refactor original code of
[`awx-demo.py`](https://gitlab.com/osiux/charlas/-/blob/develop/awx/awx-demo.py)
to permit read actions from file to control _Splinter_

## License

GNU General Public License, GPLv3.

## Author Information

This repository was created in 2022 by
 [Osiris Alejandro Gomez](https://osiux.com/), worker cooperative of
 [gcoop Cooperativa de Software Libre](https://www.gcoop.coop/).

## Virtualenv

Create virtualenv:

```shell
python3 -m venv venv
```

Activate virtualenv:

```shell
source venv/bin/activate
```

Install dependences:

```shell
pip install -r requirements.txt
```

```shell
wget https://github.com/mozilla/geckodriver/releases/download/v0.30.0/geckodriver-v0.30.0-linux64.tar.gz
tar xvf geckodriver-v0.30.0-linux64.tar.gz
sudo cp geckodriver /usr/bin
sudo chmod 755 /usr/bin/geckodriver
```

```
sudo apt install xautomation
```
